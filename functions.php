<?php
/**
 * @version    1.0
 * @package    VG Sassy Girl
 * @author     VinaGecko Team <support@vinagecko.com>
 * @copyright  Copyright (C) 2015 VinaGecko.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://vinagecko.com
 */

add_action('wp_enqueue_scripts', 'sassy_girl_child_enqueue_styles', 10000);
function sassy_girl_child_enqueue_styles() {
    wp_enqueue_style('vg-sassy-girl-child-style', get_stylesheet_directory_uri() . '/style.css' );
}